## Proxygen: Facebook's C++ HTTP Libraries

[![Build Status](https://github.com/facebook/proxygen/workflows/CI/badge.svg)](https://github.com/facebook/proxygen/actions?workflow=CI)

This project comprises the core C++ HTTP abstractions used at